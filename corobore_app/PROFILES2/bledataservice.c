/*******************************************************************************
  Filename:       bledataservice.c

*******************************************************************************/

/*********************************************************************
 * INCLUDES
 */

#include "../PROFILES2/smibot_util.h"
#include "linkdb.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "string.h"
#include "../PROFILES2/bleDataservice.h"
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <stdint.h>

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/* Service configuration values */
#define bleData_RX_DESCR       "BLE data read 220B"
#define bleData_WR_DESCR       "BLE data write 220B"

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */


// Service UUID
static CONST uint8_t bleDataServiceUUID[TI_UUID_SIZE] =
{
  TI_UUID(bleData_SERVICE_UUID),
};

// Characteristic UUID: read
static CONST uint8_t bleDataRxUUID[TI_UUID_SIZE] =
{
  TI_UUID(bleData_RX_UUID),
};

// Characteristic UUID: write
static CONST uint8_t bleDataWrUUID[TI_UUID_SIZE] =
{
  TI_UUID(bleData_WR_UUID),
};


/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

//static bleDataCBs_t *bleData_AppCBs = NULL;


/*********************************************************************
 * Profile Attributes - variables
 */

// Profile Service attribute
static CONST gattAttrType_t bleDataService = { ATT_BT_UUID_SIZE, bleDataServiceUUID };

	// Characteristic Value: read
	static uint8_t bleDataRxValue[BLE_DATA_LEN] = {0};
	static uint16_t current_data_size = BLE_DATA_LEN;

	// Characteristic Properties: data
	static uint8_t bleDataRxProps = GATT_PROP_NOTIFY;// GATT_PROP_READ | GATT_PROP_NOTIFY;

	// Characteristic Configuration: data
	static gattCharCfg_t *bleDataConfig;

	#ifdef USER_DESCRIPTION
	// Characteristic User Description: data
	static uint8_t bleDataRxUserDescr[] = bleData_RX_DESCR;
	#endif


	// Characteristic Properties: write
	static uint8_t bleDataWrProps = GATT_PROP_WRITE;

	// Characteristic Value:
	static uint8_t bleDataWr[BLE_DATA_LEN] = {0};

	#ifdef USER_DESCRIPTION
	// Characteristic User Description: configuration
	static uint8_t bleDataWrUserDescr[] = bleData_WR_DESCR;
	#endif

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t bleDataAttrTable[] =
{
  {
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8_t *)&bleDataService                 /* pValue */
  },

    // Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &bleDataRxProps
    },
      // Characteristic Value "read"
      {
        { ATT_BT_UUID_SIZE, bleDataRxUUID},
        GATT_PERMIT_READ,
        0,
        bleDataRxValue
      },

      // Characteristic configuration
      {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8_t *)&bleDataConfig
      },

#ifdef USER_DESCRIPTION
      // Characteristic User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        bleDataRxUserDescr
      },
#endif

    // Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &bleDataWrProps
    },

      // Characteristic Value "write"
      {
        { ATT_BT_UUID_SIZE, bleDataWrUUID },
        GATT_PERMIT_WRITE,
        0,
        bleDataWr
      },

#ifdef USER_DESCRIPTION
      // Characteristic User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        bleDataWrUserDescr
      },
#endif
};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t bleData_ReadAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                   uint8_t *pValue, uint16_t *pLen, 
                                   uint16_t offset, uint16_t maxLen,
                                   uint8_t method);
static bStatus_t bleData_WriteAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                    uint8_t *pValue, uint16_t len, 
                                    uint16_t offset, uint8_t method);

/*********************************************************************
 * PROFILE CALLBACKS
 */
// Log Profile Service Callbacks
static CONST gattServiceCBs_t bleDataCBs =
{
  bleData_ReadAttrCB,  // Read callback function pointer
  bleData_WriteAttrCB, // Write callback function pointer
  NULL                // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      bleData_addService
 *
 * @brief   Initializes the bleData Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @return  Success or Failure
 */
bStatus_t bleData_addService()
{
	// Allocate Client Characteristic Configuration table
	bleDataConfig = (gattCharCfg_t *)ICall_malloc(sizeof(gattCharCfg_t) *
													linkDBNumConns);
	if (bleDataConfig == NULL)
	{
		return (bleMemAllocError);
	}

	// Register with Link DB to receive link status change callback
	GATTServApp_InitCharCfg(INVALID_CONNHANDLE, bleDataConfig);


	// Register GATT attribute list and CBs with GATT Server App
	return GATTServApp_RegisterService(bleDataAttrTable,
									   GATT_NUM_ATTRS (bleDataAttrTable),
									   GATT_MAX_ENCRYPT_KEY_SIZE,
									   &bleDataCBs);
}


/*********************************************************************
 * @fn      bleData_registerAppCBs
 *
 * @brief   Registers the application callback function. Only call
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
/*bStatus_t bleData_registerAppCBs(logCBs_t *appCallbacks)
{
  if (bleData_AppCBs == NULL)
  {
    if (appCallbacks != NULL)
    {
      bleData_AppCBs = appCallbacks;
    }

    return (SUCCESS);
  }

  return (bleAlreadyInRequestedMode);
}*/

/*********************************************************************
 * @fn      bleData_setParameter
 *
 * @brief   Set a parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to write
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 *
 * @return  bStatus_t
 */
bStatus_t bleData_setParameter(uint8_t param, uint8_t len, const void *value)
{
  bStatus_t ret = SUCCESS;

  switch (param)
  {
    case BLEDATA_RX:
   // if (len == BLE_DATA_LEN)
    //{
      //memcpy(bleDataRx, value, BLE_DATA_LEN);
    	memcpy(bleDataRxValue, value, len);
      // See if Notification has been enabled
      ret = GATTServApp_ProcessCharCfg(bleDataConfig, bleDataRxValue, FALSE,
                                 	   bleDataAttrTable, GATT_NUM_ATTRS(bleDataAttrTable),
									   INVALID_TASK_ID, bleData_ReadAttrCB);
  //  }
    /*else
    {
      ret = bleInvalidRange;
    }*/
    break;

    case BLEDATA_WR:
      if (len == BLE_DATA_LEN)
      {
    	  memcpy(bleDataWr, value, BLE_DATA_LEN);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return (ret);
}


bStatus_t bleData_setSubPackage(uint16_t header, const void *value, size_t length)
{
	// To use manual notifications we need to:
	// Get handle to our characteristic
	// Determine how much data we can send, for now lets use 140 bytes

	attHandleValueNoti_t noti;
	uint16 len;
	bStatus_t status;

	// If the attribute value is longer than (ATT_MTU - 3) octets, then
	// only the first (ATT_MTU - 3) octets of this attributes value can
	// be sent in a notification.
	noti.pValue = (uint8 *)GATT_bm_alloc( 0, ATT_HANDLE_VALUE_NOTI,
	  GATT_MAX_MTU, &len );

	if ( noti.pValue != NULL )
	{
		// How many bytes did we get?
		System_printf("Got %d bytes\n", len);

		//TODO: Need to make sure len > length+2 otherwise bail out
		memcpy(noti.pValue, &header, sizeof(uint16_t));
		memcpy(noti.pValue + 2, value, length);

		noti.len = 2+length;

		// This can be done once and store the handle
		gattAttribute_t *pAttr = GATTServApp_FindAttr( bleDataAttrTable, GATT_NUM_ATTRS( bleDataAttrTable ), bleDataRxValue);
		noti.handle = pAttr->handle;

		// Send noti on conn=0
		// TODO: Should probably look if client has subsribed for notis before this... fix later
		status = GATT_Notification( 0, &noti, 0 );

		if ( status != SUCCESS ) //if noti not sent
		{
			// On failure we need to free mem
			System_printf("Failed sending noti\n");
		  GATT_bm_free( (gattMsg_t *)&noti, ATT_HANDLE_VALUE_NOTI );
		}
		else //noti sent, increment counter
		{
			System_printf("Ok sending noti\n");
		  // Everything ok, osal will free allocated mem
		}
	}

	  System_flush();

	return status;
}



// Debug version that print out data
/*
bStatus_t bleData_setSubPackage(uint16_t header, const void *value, size_t length)
{
	bStatus_t ret = SUCCESS;
	if(length > BLE_DATA_LEN-2)
		return FAILURE;

	memcpy(bleDataRx, &header, sizeof(uint16_t));
	memcpy(bleDataRx+2, value, length);
	//Need to set the current size of package
	current_data_size = length;
    // See if Notification has been enabled
	//ret = GATTServApp_ProcessCharCfg(bleDataConfig, bleDataRx, FALSE,
    //									 bleDataAttrTable, GATT_NUM_ATTRS(bleDataAttrTable),
	//  							     INVALID_TASK_ID, bleData_ReadAttrCB);

	uint8_t i;
	System_printf("This packet data: ");
	for(i=0; i< length+2; ++i)
		  {
			  System_printf("%d, ", *(bleDataRx+i));

		  }
		  System_printf("\n\n");
		  System_flush();

	return ret;
}
*/

/*********************************************************************
 * @fn      bleData_getParameter
 *
 * @brief   Get a Sensor Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 *
 * @return  bStatus_t
 */
bStatus_t bleData_getParameter(uint8_t param, void *value)
{
  bStatus_t ret = SUCCESS;

  switch (param)
  {
    case BLEDATA_RX:
      memcpy(value, bleDataRxValue, BLE_DATA_LEN);
      break;

   case BLEDATA_WR:
	   memcpy(value, bleDataWr, BLE_DATA_LEN);
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return (ret);
}

/*********************************************************************
 * @fn          bleData_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 * @param       method - type of read message 
 *
 * @return      SUCCESS, blePending or Failure
 */
static bStatus_t bleData_ReadAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                   uint8_t *pValue, uint16_t *pLen, 
                                   uint16_t offset, uint16_t maxLen,
                                   uint8_t method)
{
  uint16_t uuid;
  bStatus_t status = SUCCESS;

  // If attribute permissions require authorization to read, return error
  if (gattPermitAuthorRead(pAttr->permissions))
  {
    // Insufficient authorization
    return (ATT_ERR_INSUFFICIENT_AUTHOR);
  }

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if (offset > 0)
  {
    return (ATT_ERR_ATTR_NOT_LONG);
  }

  if (utilExtractUuid16(pAttr,&uuid) == FAILURE) 
  {
    // Invalid handle
    *pLen = 0;
    return ATT_ERR_INVALID_HANDLE;
  }

  switch (uuid)
  {
    // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
    // gattserverapp handles those reads
    case bleData_RX_UUID:
      //*pLen = BLE_DATA_LEN;
	  //memcpy(pValue, pAttr->pValue, BLE_DATA_LEN);
    	*pLen = current_data_size;
    	memcpy(pValue, pAttr->pValue, current_data_size);

    	/*System_printf("RxAtt CB l: %d d: ", *pLen);
    	    	  uint8_t ii;
    	    	  for (ii=0; ii< (*pLen) ;ii++)
    	    	  {
    	    		  System_printf(" %d, ", *(pValue+ii));
    			  }
    	    	  System_printf("\n");
    	    	  System_flush();*/

      break;

    case bleData_WR_UUID:
      *pLen = BLE_DATA_LEN;
      *pValue = *pAttr->pValue;
      break;

    default:
      *pLen = 0;
      status = ATT_ERR_ATTR_NOT_FOUND;
      break;
    }

  return (status);
}

/*********************************************************************
 * @fn      bleData_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message 
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t bleData_WriteAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                    uint8_t *pValue, uint16_t len, 
                                    uint16_t offset, uint8_t method)
{
  bStatus_t status = SUCCESS;
  uint16_t uuid;

  // If attribute permissions require authorization to write, return error
  if (gattPermitAuthorWrite(pAttr->permissions))
  {
    // Insufficient authorization
    return (ATT_ERR_INSUFFICIENT_AUTHOR);
  }

  if (utilExtractUuid16(pAttr,&uuid) == FAILURE) {
    // Invalid handle
    return ATT_ERR_INVALID_HANDLE;
  }

  switch (uuid)
  {
    case bleData_RX_UUID:
      // Should not get here
      break;


    case bleData_WR_UUID:
      // Validate the value
      // Make sure it's not a blob oper
      if (offset == 0)
      {
        /*if (len != BLE_DATA_LEN)
        {
          status = ATT_ERR_INVALID_VALUE_SIZE;
        }*/
      }
      else
      {
        status = ATT_ERR_ATTR_NOT_LONG;
      }
      // Call to the callback
      if (status == SUCCESS)
      {
		  //////////////////////////////uart2ble_relay_message(pValue, len);

		  System_printf("WrAtt CB l: %d d: ", len);
    	  uint8_t ii;
    	  for (ii=0; ii<10 ;ii++)
    	  {
    		  System_printf(" %d, ", *(pValue+ii));
		  }
    	  System_printf("\n");
    	  System_flush();


      }
      else
      {
           status = ATT_ERR_INVALID_VALUE;
      }

      break;

    case GATT_CLIENT_CHAR_CFG_UUID: // muste have this case to enable notification on the client side
      status = GATTServApp_ProcessCCCWriteReq(connHandle, pAttr, pValue, len,
                                              offset, GATT_CLIENT_CFG_NOTIFY);
      break;

    default:
      // Should never get here!
      status = ATT_ERR_ATTR_NOT_FOUND;
      break;
  }

  return (status);
}

/*********************************************************************
*********************************************************************/
